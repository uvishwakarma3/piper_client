# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import urllib
from difflib import SequenceMatcher
from piper_client.gui import MainWindow
from piper_client.gui.qt import QtGui, QtCore, QtWidgets
from piper_client.gui.utils import error_message
from piper_client.gui.widgets.cascading_config import CascadingConfig
from piper_client.core.api import PiperClientApi
from piper_client.core.utils import get_current_user_name, browse_path, piper_except_hook


class Browser(MainWindow):
    """
    An app for representing and browsing database data.
    """
    def __init__(self, parent=None):
        MainWindow.__init__(self, parent, 'Piper Browser', 'browser.ui')
        self.api = PiperClientApi()
        self.item_entity_dict = dict()  # To contain UI item-to-entity pairs.
        self.project_dict = dict()  # To contain project name-to-entity pairs.
        self.asset_type_dict = dict()  # To contain asset type name-to-entity pairs.
        self.version_item_dict = dict()  # To contain version ID-to-item index pairs.
        self.filter_checkboxes = []
        self.filters = {'status': [],
                        'asset_type': [],
                        'category': []}
        self.selected_entity = None
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.api.query_fields = ['created_by', 'status']
        self.api.query_fields_reset = False
        # Add projects to the project combo box:
        self.projectComboBox.addItem('')
        for project_entity in self.api.get(entity_type='Project'):
            self.project_dict[project_entity.long_name] = project_entity
            self.projectComboBox.addItem(project_entity.long_name)

        # Add asset type checkboxes to the asset type filters area:
        asset_types = self.api.get(entity_type='AssetType')
        for asset_type in asset_types:
            self.asset_type_dict[asset_type.long_name] = asset_type
            checkbox = QtWidgets.QCheckBox(asset_type.long_name)
            self.filter_checkboxes.append(checkbox)
            checkbox.setChecked(True)
            checkbox.stateChanged.connect(lambda checked, asset_type_id=asset_type.id:
                                          self.filter_tree_items(attribute='asset_type',
                                                                 value=asset_type_id,
                                                                 check_state=checked))
            self.assetTypeFiltersLayout.addWidget(checkbox)

        # Add category checkboxes to the category filter area:
        for category in self.api.get(entity_type='Category'):
            checkbox = QtWidgets.QCheckBox(category.long_name)
            self.filter_checkboxes.append(checkbox)
            checkbox.setChecked(True)
            checkbox.stateChanged.connect(lambda checked, value=category.id:
                                          self.filter_tree_items(attribute='category',
                                                                 value=value,
                                                                 check_state=checked))
            self.categoryFiltersLayout.addWidget(checkbox)

        # Add status checkboxes to the status filters area:
        for status in self.api.get(entity_type='Status'):
            checkbox = QtWidgets.QCheckBox(status.long_name)
            self.filter_checkboxes.append(checkbox)
            checkbox.setChecked(True)
            checkbox.stateChanged.connect(lambda checked, value=status.id:
                                          self.filter_tree_items(attribute='status',
                                                                 value=value,
                                                                 check_state=checked))
            self.statusFiltersLayout.addWidget(checkbox)

        # Set the user filter to the current user:
        self.userFilterLineEdit.setText(get_current_user_name())

        # Set up the asset tree view model:
        self.item_model = QtGui.QStandardItemModel()
        self.assetTreeView.setModel(self.item_model)
        self.refresh_tree_data()

    def refresh_tree_data(self):
        """
        Setup the asset tree data.
        :return: None;
        """
        self.item_model.clear()
        self.clear_details_layout()
        # Disable all signals while altering the asset tree:
        for checkbox in self.filter_checkboxes:
            checkbox.blockSignals(True)
            checkbox.setChecked(True)
            checkbox.blockSignals(False)
        # Set up the default header and the default column sizes:
        self.item_model.setHorizontalHeaderLabels(['Entity', 'Type', 'Created by', 'Status'])
        self.assetTreeView.header().resizeSection(0, 250)
        self.assetTreeView.header().resizeSection(1, 80)
        self.assetTreeView.header().resizeSection(2, 100)
        self.assetTreeView.header().resizeSection(3, 80)
        self.build_tree_data()
        # Show all of the fields in the tree:
        # self.assetTreeView.expandAll()

    def clear_details_layout(self):
        """
        Remove all data from the selection details area.
        :return: None;
        """
        for i in range(self.selectionDetailsLayout.count()):
            self.selectionDetailsLayout.itemAt(i).widget().deleteLater()

    def slot_item_clicked(self, index):
        """
        Commands to execute when a tree item has been clicked.
        :param index: the index of the item clicked;
        :return: None;
        """
        # Recreate the item's unique ID:
        item_id = '{0}{1}'.format(index.internalId(), index.row())

        if item_id in self.item_entity_dict:
            # Find the item's corresponding entity and display all of its attributes and their respective values:
            if self.selected_entity and self.item_entity_dict[item_id]['entity']['id'] == self.selected_entity.id:
                return

            self.clear_details_layout()
            self.selected_entity = self.item_entity_dict[item_id]['entity']

            # Load entity thumbnail:
            thumbnail = QtWidgets.QLabel()
            data = urllib.urlopen(self.selected_entity.thumbnail_spec).read()
            pixmap = QtGui.QPixmap()
            pixmap.loadFromData(data)
            thumbnail.setPixmap(pixmap)
            self.selectionDetailsLayout.addWidget(thumbnail)
            thumbnail.show()

            # Add entity page URL:
            url = QtWidgets.QLabel('Page: <a style="color: #348bc9;text-decoration: none;" href="{0}">{0}</a>'.format(
                self.selected_entity.url))
            url.setOpenExternalLinks(True)
            self.selectionDetailsLayout.addWidget(url)

            for key in sorted(self.selected_entity.display_fields.keys()):
                if key == 'Thumbnail':
                    continue
                value = self.selected_entity.display_fields[key]
                if value:
                    if 'url' in value:
                        label = QtWidgets.QLabel(
                            '{0}: <a style="color: #348bc9;text-decoration: none;" href="{1}">{2}</a>'.format(
                                key,
                                self.api.absolute_url(
                                    value['url']),
                                value['value']))
                        label.setOpenExternalLinks(True)
                    else:
                        label = QtWidgets.QLabel('{0}: {1}'.format(key, value['value']))
                        label.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
                    self.selectionDetailsLayout.addWidget(label)

            if self.selected_entity.type == 'Version':
                label = QtWidgets.QLabel('Files: {}'.format(', '.join([file.format
                                                                       for file in self.selected_entity.files])))
                label.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
                self.selectionDetailsLayout.addWidget(label)

                show_dependency_btn = QtWidgets.QPushButton('Show Dependencies')
                self.selectionDetailsLayout.addWidget(show_dependency_btn)
                show_dependency_btn.clicked.connect(lambda button=show_dependency_btn: self.show_version_dependencies(
                    version=self.selected_entity,
                    button=button))
            if not self.item_entity_dict[item_id]['loaded'] and self.selected_entity.type != 'Project':
                for child_entity in self.selected_entity.children:
                    self.append_data_tree_row(parent_item=self.item_entity_dict[item_id]['item'], entity=child_entity)
                self.item_entity_dict[item_id]['loaded'] = True

    def show_version_dependencies(self, version, button):
        """
        A slot method for creating a tree widget to display a given version's dependencies.
        :param version: a version entity;
        :param button: a QPushButton;
        :return: None;
        """
        tree_widget = QtWidgets.QTreeWidget()
        tree_widget.setHeaderItem(QtWidgets.QTreeWidgetItem(['Entity', 'Resource', 'Version', 'Page']))
        if version.dependency:
            dependencies = version.dependency.dependencies
            if dependencies:
                for dependency in dependencies:
                    self.append_to_dependency_tree(parent_item=tree_widget, entity=dependency)
            button.setHidden(True)
        self.selectionDetailsLayout.addWidget(tree_widget)

    def append_to_dependency_tree(self, parent_item, entity):
        """
        Add a data row to the data tree and store its main item under a unique ID for future reference.
        :param parent_item: the parent item of the item to be created;
        :param entity: the entity to be represented;
        :return: a newly created data tree item;
        """

        item = QtWidgets.QTreeWidgetItem(parent_item)
        #  find_btn = QtWidgets.QPushButton('Find', parent_item)
        #  find_btn.clicked.connect(lambda version=entity: self.find_version_item(version=version))

        url = QtWidgets.QLabel('<a style="color: #348bc9;text-decoration: none;" href="{}">URL</a>'.format(entity.url),
                               parent_item)
        url.setOpenExternalLinks(True)

        parent_item.setItemWidget(item, 0, QtWidgets.QLabel('{0} {1}'.format(entity.resource.link.type,
                                                                             entity.resource.link.long_name),
                                                            parent_item))
        parent_item.setItemWidget(item, 1, QtWidgets.QLabel(entity.resource.long_name, parent_item))
        parent_item.setItemWidget(item, 2, QtWidgets.QLabel(entity.long_name, parent_item))
        parent_item.setItemWidget(item, 3, url)

        return item

    def find_version_item(self, version):
        """
        Find an item associated with a given version entity.
        :param version: a version entity;
        :return: None;
        """
        item = self.version_item_dict[version.id]
        self.assetTreeView.setCurrentIndex(item.index())
        self.slot_item_clicked(index=item.index())

    def slot_item_double_clicked(self, index):
        """
        Commands to be executed when a tree item has been double clicked.
        :param index: the index of the double-clicked item;
        :return: None;
        """
        # Recreate the item's unique ID:
        item_id = '{0}{1}'.format(index.internalId(), index.row())
        if item_id in self.item_entity_dict:
            # Find the item's corresponding entity, determine its path on the disk and locate it in the file explorer:
            entity = self.item_entity_dict[item_id]['entity']
            path = entity.publish_path

            if os.path.exists(path):
                browse_path(path)
            else:
                msg = 'Could not find publish path %s for %s %s, unique ID: %s' % (path,
                                                                                   entity.type.lower(),
                                                                                   entity.name,
                                                                                   entity.id)
                error_message(self, msg)

    def build_tree_data(self):
        """
        Represent the database as items in the UI's data tree.
        :return: None;
        """
        selected_project = self.projectComboBox.currentText()
        if not selected_project:
            return

        self.item_entity_dict = {}
        # Create an invisible root item for the data tree:
        data_tree_root = self.item_model.invisibleRootItem()

        # Create the project item:
        project_entity = self.project_dict[selected_project]

        project_item = self.append_data_tree_row(parent_item=data_tree_root, entity=project_entity)

        # Create tree branches to separate assets from sequences:
        assets_item = QtGui.QStandardItem('Assets')
        sequences_item = QtGui.QStandardItem('Sequences')
        project_item.appendRow([sequences_item, QtGui.QStandardItem(''),
                                QtGui.QStandardItem(''), QtGui.QStandardItem('')])
        project_item.appendRow([assets_item, QtGui.QStandardItem(''), QtGui.QStandardItem(''), QtGui.QStandardItem('')])
        if project_entity.episodic:
            episodes_item = QtGui.QStandardItem('Episodes')
            project_item.appendRow(
                [episodes_item, QtGui.QStandardItem(''), QtGui.QStandardItem(''), QtGui.QStandardItem('')])

        # Create asset/resource/version items:
        for asset_entity in project_entity.assets:
            asset_item = self.append_data_tree_row(parent_item=assets_item, entity=asset_entity)

        # Create sequence/shot/resource/version items:
        for sequence_entity in project_entity.sequences:
            sequence_item = self.append_data_tree_row(parent_item=sequences_item, entity=sequence_entity)

    def append_data_tree_row(self, parent_item, entity):
        """
        Add a data row to the data tree and store its main item under a unique ID for future reference.
        :param parent_item: the parent item of the item to be created;
        :param entity: the entity to be represented;
        :return: a newly created data tree item;
        """
        item = QtGui.QStandardItem(entity.long_name)
        parent_item.appendRow([item,
                              QtGui.QStandardItem(entity.type),
                              QtGui.QStandardItem(entity.created_by),
                              QtGui.QStandardItem(entity.status.long_name)])
        index = item.index()
        # Create a unique index ID based on the index internal ID and row number:
        index_id = '{0}{1}'.format(index.internalId(), index.row())
        # Store the item-to-entity relationship for future reference:
        self.item_entity_dict[index_id] = dict()
        self.item_entity_dict[index_id]['item'] = item
        self.item_entity_dict[index_id]['entity'] = entity
        self.item_entity_dict[index_id]['parent'] = parent_item
        self.item_entity_dict[index_id]['hidden'] = False
        self.item_entity_dict[index_id]['loaded'] = False

        if entity.type == 'Version':
            self.version_item_dict[entity.id] = item

        for attribute, values in self.filters.items():
            if values:
                for value in values:
                    self.filter_tree_items(attribute=attribute, value=value, item_id=index_id)
        return item

    def set_tree_item_visibility(self, item, hidden, silent=False):
        """
        Show/hide an item in the data tree.
        :param item: the item to show/hide;
        :param hidden: a boolean to determine if the item should be hidden or not;
        :param silent: a boolean to determine if the hidden state should be reflected in the item-entity dictionary;
        :return: None;
        """
        item_id = '{0}{1}'.format(item.index().internalId(), item.index().row())
        parent_item = self.item_entity_dict[item_id]['parent']
        self.assetTreeView.setRowHidden(item.row(), parent_item.index(), hidden)
        if not silent:
            self.item_entity_dict[item_id]['hidden'] = hidden

    def filter_tree_items(self, attribute, value, check_state=False, item_id=None):
        """
        Show/hide certain data tree items based on the attribute-value pair of their respective entities.
        :param attribute: the entity's attribute name (string);
        :param value: the value of the entity's attribute (string);
        :param check_state: the check state, from which to determine the hidden state;
        :param item_id: a specific item id (int) to filter;
        :return: None;
        """
        if check_state:
            hidden = False
            self.filters[attribute].remove(value)
        else:
            hidden = True
            if value not in self.filters[attribute]:
                self.filters[attribute].append(value)

        if item_id:
            item_ids = [item_id]
        else:
            item_ids = self.item_entity_dict

        for i in item_ids:
            item = self.item_entity_dict[i]['item']
            entity = self.item_entity_dict[i]['entity']

            # Ignore projects as they should be visible at all times:
            if entity.type is 'Project':
                continue

            if entity[attribute] and entity[attribute].id == value:
                self.set_tree_item_visibility(item=item, hidden=hidden)

    def filter_by_user(self):
        """
        Filter all data tree items by user.
        :return: None;
        """
        hidden = not self.userFilterCheckBox.isChecked()
        user = self.userFilterLineEdit.text().lower()
        if hidden:
            for item_id in self.item_entity_dict:
                item = self.item_entity_dict[item_id]['item']
                parent_item = self.item_entity_dict[item_id]['parent']
                hidden = self.item_entity_dict[item_id]['hidden']
                self.assetTreeView.setRowHidden(item.row(), parent_item.index(), hidden)
        else:
            for item_id in self.item_entity_dict:
                item = self.item_entity_dict[item_id]['item']
                entity = self.item_entity_dict[item_id]['entity']
                item_hidden = self.item_entity_dict[item_id]['hidden']
                # Ignore projects as they should be visible at all times:
                if entity.type is 'Project':
                    continue
                if entity.created_by == user and not item_hidden:
                    self.set_tree_item_visibility(item=item, hidden=False, silent=True)
                else:
                    self.set_tree_item_visibility(item=item, hidden=True, silent=True)

    def user_filter_change_cmd(self):
        """
        Apply the user filter if the user filter check box is enabled.
        :return: None;
        """
        if self.userFilterCheckBox.isChecked():
            self.filter_by_user()

    def find_item(self, omit=None):
        """
        Select the data tree item that most closely matches the find field input.
        :param omit: an item to omit from the selection process;
        :return: None;
        """
        identifier = self.findLineEdit.text().lower()  # The find field input.
        match_ratio = 0.0  # To contain the percentage of similarity between a given item and the find field input.
        for item_id in self.item_entity_dict:
            item = self.item_entity_dict[item_id]['item']
            if item is not omit:
                # Determine the similarity between the two strings:
                ratio = SequenceMatcher(None, identifier, item.text()).ratio()
                # If there is a higher similarity ratio, select the item and update the current match ratio:
                if ratio > match_ratio:
                    match_ratio = ratio
                    self.assetTreeView.setCurrentIndex(item.index())

    def find_next_cmd(self):
        """
        Find the next most similar item in the data tree based on the find field input.
        :return: None;
        """
        selection = self.assetTreeView.selectedIndexes()
        if selection:
            index = selection[0]
            item = index.model().itemFromIndex(index)
            self.find_item(omit=item)

    def run_cascading_config(self):
        if self.selected_entity:
            cascading_config_dialog = CascadingConfig(parent=self,
                                                      entity_type=self.selected_entity.type,
                                                      entity_id=self.selected_entity.id)
            cascading_config_dialog.exec_()
        else:
            return error_message(self, 'No entity selected to configure.')

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.projectComboBox.currentIndexChanged.connect(self.refresh_tree_data)
        self.assetTreeView.clicked.connect(self.slot_item_clicked)
        self.assetTreeView.doubleClicked.connect(self.slot_item_double_clicked)
        self.findLineEdit.textChanged.connect(self.find_item)
        self.findNextBtn.clicked.connect(self.find_next_cmd)
        self.userFilterCheckBox.stateChanged.connect(self.filter_by_user)
        self.userFilterLineEdit.textChanged.connect(self.user_filter_change_cmd)
        self.refreshBtn.clicked.connect(self.refresh_tree_data)
        self.configureBtn.clicked.connect(self.run_cascading_config)
        self.closeBtn.clicked.connect(self.close)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QtWidgets.QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show(parent=None):
    """
    Show the UI.
    :param parent: parent Qt window;
    :return: None;
    """
    sys.excepthook = piper_except_hook
    browser = Browser(parent=parent)
    browser.show()


if __name__ == '__main__':
    run()
