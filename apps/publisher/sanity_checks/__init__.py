# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import importlib
from piper_client.core.logger import get_logger


def get_sanity_checks():
    """
    Load an return a DCC-related API module.
    :param dcc: the name of the related DCC;
    :return: a module object, e.g. core.dcc.maya_api;
    """
    sanity_check_modules = []
    for file_name in os.listdir(os.path.abspath(os.path.join(os.path.dirname(__file__)))):
        if not file_name.startswith('_') and file_name.endswith('.py'):
            module = importlib.import_module('.{}'.format(file_name.replace('.py', '')), package=__package__)
            if hasattr(module, 'SanityCheck'):
                sanity_check_modules.append(module.SanityCheck)
    return sanity_check_modules


class SanityCheck(object):
    """
    The base sanity check class.
    Contains variables and methods required to run a Piper sanity check prior to publishing.
    """

    name = None  # a name for the sanity check;
    description = None  # a description for the sanity check;
    required = False  # whether the sanity check is required/can be skipped by the user;
    checked = False  # whether the sanity check is enabled by default;
    priority = 1  # the priority of this check;

    def __init__(self, dcc_api, resource, parent=None):
        self.logger = get_logger('Sanity Check "{}"'.format(self.name))
        self.dcc_api = dcc_api
        self.resource = resource
        self.parent = parent
        self.piper_api = resource.api

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        return True

    def run(self):
        """
        The main function executed by the sanity check.
        :return: True/False;
        """
        return True

    def fix(self):
        """
        A method that attempts a fix should the sanity check fail (return False) on run.
        :return: True/False;
        """
        return True
