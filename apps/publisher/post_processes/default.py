# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from . import PostProcess as PostProcessBase


class PostProcess(PostProcessBase):

    name = 'Default'
    description = 'Default Piper post-publishing process.'
    required = True
    checked = True
    priority = 100

    def __init__(self, **kwargs):
        super(PostProcess, self).__init__(**kwargs)

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        return True

    def run(self):
        """
        The main function executed by the process.
        :return: True/False;
        """
        # Un-group the publishing group and save the file:
        group = self.version.work_file.name
        self.dcc_api.ungroup(group)
        self.dcc_api.save_file(file_path=self.version.work_file.work_path)
        return True
