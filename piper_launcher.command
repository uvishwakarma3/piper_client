echo Starting Piper Launcher...
export PIPER=$(dirname "${BASH_SOURCE[0]}")
export PIPER_TEMP=/tmp/
source $PIPER/piper_env.command
python $PIPER/run.py