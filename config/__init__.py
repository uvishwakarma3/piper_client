# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""
__version__ = '1.0.1'

import os
import sys

# Set static global variables:
PIPER = os.path.abspath(os.path.join(os.path.dirname(__file__), '..')).replace('\\', '/')
PIPER_INIT = os.path.abspath(os.path.join(PIPER, '..'))
sys.path.append(PIPER_INIT)

ICONS_PATH = os.path.join(PIPER, 'gui', 'images', 'icons').replace('\\', '/')
QSS_PATH = os.path.join(PIPER, 'gui', 'qss').replace('\\', '/')
PIPER_LAUNCHER_ROOT = os.path.join(PIPER, 'apps', 'launcher')
MODULES_PATH = PIPER + '/studio/modules'

# Setting Piper environment variables:
os.environ['PIPER'] = PIPER
os.environ['PIPER_INIT'] = PIPER_INIT
os.environ['PIPER_LAUNCHER_MAIN'] = os.path.join(PIPER_LAUNCHER_ROOT, 'main.py')
os.environ['PIPER_VENV'] = os.path.join(PIPER, 'venv_{}'.format(sys.platform)).replace('\\', '/')
os.environ['PIPER_VERSION'] = __version__

# Set Python executable based on the OS:
if sys.platform == "win32":
    os.environ['PIPER_PYTHON'] = os.path.join(os.environ['PIPER_VENV'], 'Scripts', 'python.exe')
    os.environ['PIPER_PYTHONW'] = os.path.join(os.environ['PIPER_VENV'], 'Scripts', 'pythonw.exe')
    PIPER_SITE_PACKAGES = os.path.join(os.environ['PIPER_VENV'], 'Lib', 'site-packages')
else:
    os.environ['PIPER_PYTHON'] = os.path.join(os.environ['PIPER_VENV'], 'bin', 'python')
    os.environ['PIPER_PYTHONW'] = os.path.join(os.environ['PIPER_VENV'], 'bin', 'pythonw')
    PIPER_SITE_PACKAGES = os.path.join(os.environ['PIPER_VENV'],
                                       'lib',
                                       '{}'.format([x for x in os.listdir(
                                           os.path.join(os.environ['PIPER_VENV'], 'lib'))
                                                    if not x.startswith('.')][0]),
                                       'site-packages')
os.environ['PIPER_SITE_PACKAGES'] = PIPER_SITE_PACKAGES

# Get studio configuration:
from piper_client.core.utils import read_json, write_json


class CredentialsConfig(object):
    """
    A class to fetch and store studio configuration data.
    """
    def __init__(self):
        self.data = {}
        self.source = os.path.join(os.environ['PIPER_TEMP'], '_piper_config.json')
        self.reload()

    def reload(self):
        """
        Reload the studio config file.
        :return: None;
        """
        if os.path.isfile(self.source):
            self.data = read_json(self.source)
            os.environ['PIPER_SERVER'] = self.data['piper_server']


credentials_config = CredentialsConfig()


SETTINGS_JSON = os.path.join(os.path.dirname(__file__), 'settings.json')
if not os.path.exists(SETTINGS_JSON):
    settings = {'stylesheet': 'light.qss'}
    write_json(settings, SETTINGS_JSON)
else:
    settings = read_json(os.path.join(os.path.dirname(__file__), 'settings.json'))
