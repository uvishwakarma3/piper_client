#!/bin/bash
echo Setting up Python...
curl --remote-name http://piperpipeline.com/resources/src/Python-3.7.3.tgz
tar -xzvf Python-3.7.3.tgz
cd ./Python-3.7.3
./configure --prefix=$PIPER/resources/$PIPER_OS/Python37 --enable-so --with-included-apr
make
make install
cd ..
echo Python has been set up.