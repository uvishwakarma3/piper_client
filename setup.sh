#!/bin/bash
echo Setting up Piper Client, please wait...
export PIPER=$PWD
export PIPER_OS=linux
cd $PIPER/resources/linux
source $PIPER/resources/linux/setup_python.sh
source $PIPER/resources/linux/setup_nodejs.sh
cd $PIPER
$PIPER/resources/linux/Python37/bin/python3.7 -m virtualenv venv_linux -p $PIPER/resources/linux/Python37/bin/python3.7
source $PIPER/venv_linux/bin/activate
pip install -r requirements.txt
cd $PIPER/apps/launcher
$PIPER/resources/linux/NodeJS/lib/node_modules/npm/bin/npm-cli.js install . --scripts-prepend-node-path && $PIPER/resources/linux/NodeJS/lib/node_modules/npm/bin/npm-cli.js run package-linux --scripts-prepend-node-path
cd $PIPER
echo Piper Client setup completed.