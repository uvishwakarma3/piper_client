# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
from piper_client.gui import Widget
from piper_client.core.api import PiperClientApi
from piper_client.gui.qt import QtGui, QtWidgets, QtCore
from piper_client.gui.utils import set_combo_box_text, load_qt_ui
from piper_client.core.utils import environment_check


class ContextWidget(Widget):
    """
    An widget for setting and getting an asset or shot context.
    """
    context_changed = QtCore.Signal(object)  # a signal to be emitted each time the context is changed;

    def __init__(self, parent=None):
        Widget.__init__(self, parent, 'Piper Context Widget', 'context_widget.ui')
        self._data = {
            'projects': {},
            'asset_types': {},
            'assets': {},
            'sequences': {},
            'shots': {}
        }  # To store label-to-entity references.
        self.api = PiperClientApi()
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None;
        """
        # Clear in case of refresh:
        self.projectComboBox.clear()
        self.assetTypeComboBox.clear()

        # Populate with projects:
        for project_entity in self.api.get(entity_type='Project'):
            self._data['projects'][project_entity.label] = project_entity
            self.projectComboBox.addItem(project_entity.label)
        if environment_check('PIPER_CONTEXT_PROJECT'):
            set_combo_box_text(combo_box=self.projectComboBox, text=os.environ['PIPER_CONTEXT_PROJECT'])

        # Populate with asset types:
        for asset_type_entity in self.api.get(entity_type='AssetType'):
            self._data['asset_types'][asset_type_entity.label] = asset_type_entity
            self.assetTypeComboBox.addItem(asset_type_entity.label)
        if environment_check('PIPER_CONTEXT_ASSET_TYPE'):
            set_combo_box_text(combo_box=self.assetTypeComboBox, text=os.environ['PIPER_CONTEXT_ASSET_TYPE'])
            if self.parent.assembly:
                set_combo_box_text(combo_box=self.parent.assemblyAssetTypeComboBox,
                                   text=os.environ['PIPER_CONTEXT_ASSET_TYPE'])

        self.re_populate_all()
        self._store_context_type()

    def re_populate_all(self):
        """
        Re-populate various UI elements with data.
        :return: None;
        """

        self.populate_sequences()
        if environment_check('PIPER_CONTEXT_SEQUENCE'):
            set_combo_box_text(combo_box=self.sequenceComboBox, text=os.environ['PIPER_CONTEXT_SEQUENCE'])

        self.populate_shots()
        if environment_check('PIPER_CONTEXT_SHOT'):
            set_combo_box_text(combo_box=self.shotComboBox, text=os.environ['PIPER_CONTEXT_SHOT'])

        self.populate_assets(context=True)
        if environment_check('PIPER_CONTEXT_ASSET'):
            set_combo_box_text(combo_box=self.assetComboBox, text=os.environ['PIPER_CONTEXT_ASSET'])

        if environment_check('PIPER_CONTEXT_TYPE') and os.environ['PIPER_CONTEXT_TYPE']:
            self.contextTabWidget.setCurrentIndex(1)
        else:
            os.environ['PIPER_CONTEXT_TYPE'] = 'shot'

    def populate_sequences(self):
        """
        Populate the UI's sequence combo box with all the sequences of the current project.
        :return: None;
        """
        self.sequenceComboBox.clear()
        selected_project = self.projectComboBox.currentText()
        if selected_project:
            project_entity = self._data['projects'][selected_project]
            for sequence_entity in self.api.get(entity_type='Sequence', filters={'project': project_entity}):
                # Store the sequence in the context data dictionary for future reference:
                self._data['sequences'][sequence_entity.label] = sequence_entity
                self.sequenceComboBox.addItem(sequence_entity.label)

    def populate_shots(self):
        """
        Populate the shot combo box with all the shots of the current project and sequence.
        :return: None;
        """
        self.shotComboBox.clear()
        selected_sequence = self.sequenceComboBox.currentText()
        if selected_sequence:
            for shot_entity in self.api.get(entity_type='Shot',
                                            filters={'sequence': self._data['sequences'][selected_sequence]}):
                # Store the shot in the context data dictionary for future reference:
                self._data['shots'][shot_entity.label] = shot_entity
                self.shotComboBox.addItem(shot_entity.label)

    def populate_assets(self, context=True):
        """
        Add assets to the UI.
        :param context: a boolean check to determine if the method should populate the context or the assembly asset
        UI elements if available;
        :return: None;
        """

        # Get get the asset type entity depending on the asset context:
        if context:
            self.assetComboBox.clear()
            asset_type_entity = self._data['asset_types'][self.assetTypeComboBox.currentText()]

        selected_project = self.projectComboBox.currentText()
        if selected_project:
            # Get the assets of the selected project and add them to the UI:
            for asset_entity in self.api.get(entity_type='Asset',
                                             filters={'project': self._data['projects'][selected_project]}):
                if asset_entity.asset_type == asset_type_entity:
                    # Store the asset in the context data dictionary for future reference:
                    self._data['assets'][asset_entity.label] = asset_entity
                    if context:
                        self.assetComboBox.addItem(asset_entity.label)

    def _store_context_type(self):
        """
        Store the current context type - 'asset' or 'shot' - as an environment variable.
        :return: None;
        """
        # Check what the context type is based on the active context tab:
        if self.contextTabWidget.currentIndex():
            os.environ['PIPER_CONTEXT_TYPE'] = 'asset'
        else:
            os.environ['PIPER_CONTEXT_TYPE'] = 'shot'
        self.context_changed.emit(True)

    @property
    def current_project(self):
        """
        Get the current project entity.
        :return: a project entity if there is a selected project, otherwise - None;
        """
        selected_project = self.projectComboBox.currentText()
        if selected_project:
            return self._data['projects'][selected_project]
        return None

    @property
    def current_entity(self):
        """
        Get the currently selected asset or shot entity.
        :return: a shot or asset entity if selected, otherwise - None;
        """
        if os.environ['PIPER_CONTEXT_TYPE'] == 'asset':
            selected_asset = self.assetComboBox.currentText()
            if selected_asset:
                return self._data['assets'][selected_asset]
        else:
            selected_shot = self.shotComboBox.currentText()
            if selected_shot:
                return self._data['shots'][selected_shot]
        return None

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.projectComboBox.currentIndexChanged.connect(self.re_populate_all)
        self.sequenceComboBox.currentIndexChanged.connect(self.populate_shots)
        self.contextTabWidget.currentChanged.connect(self._store_context_type)
        self.assetTypeComboBox.currentIndexChanged.connect(lambda: self.populate_assets(context=True))
        self.shotComboBox.currentIndexChanged.connect(lambda: self.context_changed.emit(True))
        self.assetComboBox.currentIndexChanged.connect(lambda: self.context_changed.emit(True))
